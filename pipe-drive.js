

module.exports = function(RED) {
    function LowerCaseNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {

            const https = require('https');
            https.get('https://companydomain.pipedrive.com/api/v1/deals/4927?api_token=db905d67170de53ec543a60cce8d5d176b57c598', (resp) => {
                let data = '';

                resp.on('data', (chunk) => {
                    data += chunk;
                });

                resp.on('end', () => {
                    msg.status =  true
                    msg.payload = data
                    node.send(msg)
                });

            }).on("error", (err) => {
                msg.status = false
                msg.payload = err
                node.send(msg)
            });
            node.send(msg);
        });
    }
    RED.nodes.registerType("lower-case",LowerCaseNode);
}
